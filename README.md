Flashcards - projekt wykonany w ramach przedmiotu Zaawansowane Techniki Internetowe

## Uruchomienie
Aby uruchomić aplikację należy pobrać Node.js
https://nodejs.org/en/download/

### `npm install`
Instalacja zależności

### `npm start`
Uruchomienie aplikacji na lokalnym porcie 3000.


