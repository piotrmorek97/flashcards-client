import React, { Fragment } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Header from './components/layout/Header'
import About from './components/About'
import FlashcardSet from './components/create/FlashcardSet'
import FlashcardSlider from './components/learn/FlashcardSlider'
import Home from './components/Home'

import FlashcardState from './context/flashcards/FlashcardState'

function App() {
  return (
    <FlashcardState>
      <Router>
        <div className="App">
          <Header />
          <Fragment>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/create" component={FlashcardSet}/>
              <Route path="/learn" component={FlashcardSlider}/>
              <Route path="/about" component={About}/>
            </Switch>
          </Fragment>
        </div>
      </Router>
    </FlashcardState>
  );
}

export default App;
