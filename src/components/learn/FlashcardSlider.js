import React, { useState, useContext } from 'react'
import FlashcardContext from '../../context/flashcards/flashcardContext'

const FlashcardSlider = () => {
  const flashcardContext = useContext(FlashcardContext);

  const { flashcards } = flashcardContext;

  // Counter to slide through flashcards
  const [counter, setCounter] = useState(0);

  // Flip the flashcard, defult is term -> flip -> definition
  const [flip, setFlip] = useState(true);

  // flip flashcard on Click
  const flipFlashcard = () => setFlip(!flip);

  // Incrementing counter logic
  const incrementCounter = () => {
    if(counter < flashcards.length - 1){
      setCounter(counter + 1);
    } else {
      setCounter(0);
    }
    // To display term first
    setFlip(true);
  };
  
  // Decrementing coutner logic
  const decrementCounter = () => {
    if(counter > 0) {
      setCounter(counter - 1);
    } else {
      setCounter(flashcards.length - 1);
    }
    // To display term first
    setFlip(true);
  };

  // Get flashcard by couter
  const getFlashcard = (x) => flashcards.length > 0 ? flashcards[x] : { term: 'default', definition: 'default'}

  return (
    <div className="d-flex flex-column align-items-center">
      <h1 className="h3 mt-4"> Flashcards </h1>
      <div className="bg-light text-dark mt-3 mb-3" style={flashcard} onClick={flipFlashcard}>
        <h3>
          {
            flip ? getFlashcard(counter).term : getFlashcard(counter).definition
          } 
        </h3>
        <div style={turnFlashcard}>
          <i className="fas fa-undo"></i>
          <span> click to flip </span>
        </div>
      </div>
      <div className="d-flex align-items-center" style={flashcardNavigation}>
        <i className="fas fa-chevron-left" style={arrow} onClick={decrementCounter}/>
        <span> {counter + 1} </span> 
        <span> / </span>
        <span> {flashcards.length} </span>
        <i className="fas fa-chevron-right" style={arrow} onClick={incrementCounter}/>
      </div>
    </div>
  )
}

const flashcard = {
  display: 'flex',
  position: 'relative',
  alignItems: 'center',
  justifyContent: 'center',
  width: '500px',
  height: '350px',
  padding: '2rem',
  margin: 'auto',
  borderRadius: '1rem',
  boxShadow: '0px 0px 15px 2px #ccc'
}

const flashcardNavigation = {
  fontSize: '1.25rem',
}

const arrow = {
  fontSize: '1.25rem',
  padding: '0 1rem',
  cursor: 'pointer'
}

const turnFlashcard = {
  position: 'absolute',
  top: '5%',
  right: '5%',
  color: '#ccc'
}

export default FlashcardSlider;
