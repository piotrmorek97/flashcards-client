import React from 'react'

const About = () => {
  return (
    <div className="pt-2 container">
      <h1> Flashcard App</h1>
      <p> Client for FlashcardApp made for ZTI laboratory classes</p>
      <p> 2020 </p>
    </div>
  )
}

export default About
