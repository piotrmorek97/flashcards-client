import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div className="landing-page-container">
      <div className="landing-page">
        <h1 className="title"> Flashcards </h1>
        <p className="description"> Create your own set and learn it quickly. This is simple web app project for ZTI classes. </p>
        <div className="options">
          <Link to="/create">
            <button className="btn btn-primary"> Create set </button>
          </Link>
          <Link to="/learn">
            <button className="btn btn-primary"> Learn </button>
          </Link>
        </div>
      </div>
    </div>
    
  )
}

export default Home;
