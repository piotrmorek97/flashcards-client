import React from 'react'
import spinner from '../../img/spinner.gif'

const Spinner = () => {
  return (
    <div className="d-flex justify-content-center">
      <img src={spinner} alt="loading..." style={spinnerStyle}/>
    </div>
  )
}

const spinnerStyle = {
  width: '40px'
}

export default Spinner
