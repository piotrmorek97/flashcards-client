import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary px-4">
      <span className="navbar-brand"> Flashcards </span>
      <div className="ml-auto" id="navbar-nav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link to="/" className="nav-link"> Home </Link>
          </li>
          <li className="nav-item">
            <Link to="/create" className="nav-link"> Create </Link>
          </li>
          <li className="nav-item">
            <Link to="/learn" className="nav-link"> Learn </Link>
          </li>
          <li className="nav-item">
            <Link to="/about" className="nav-link"> About </Link>
          </li>
        </ul>
      </div>
    </nav>
  )
}

export default Header;
