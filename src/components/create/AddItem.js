import React, { useState, useContext } from 'react'
import FlashcardContext from '../../context/flashcards/flashcardContext'

const AddItem = () => {
  const flashcardContext = useContext(FlashcardContext);

  const { addFlashcardItem } = flashcardContext;

  const [state, setState] = useState({
    term: '',
    definition: ''
  });

  // when change inputs, state is reloaded
  const onChange = (e) => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    })
  }

  // submit form and addNewItem using context
  const onSubmit = (e) => {
    e.preventDefault();
    addFlashcardItem(state);
    setState({
      term: '',
      definition: ''
    });
  }

  return (
    <div style={formStyle}>
      <form className="d-flex align-items-center" onSubmit={onSubmit}>
        <input 
          type="text"
          name="term" 
          className="form-control" 
          style={formInput}
          onChange={onChange}
          value={state.term} 
          placeholder="Term"
        />
        <input 
          type="text"
          name="definition" 
          className="form-control mx-4" 
          style={formInput}
          onChange={onChange}
          value={state.definition} 
          placeholder="Definition"
        />
        <input 
          type="submit" 
          value="Submit" 
          className="btn btn-primary" 
          style={formBtn} 
        />
      </form>
    </div>
  )
}

const formInput = {
  flex: '4'
}

const formBtn = {
  flex: '2',
  height: '2.5rem'
}

const formStyle = {
  border: '1px dashed #ccc',
  borderRadius: '0.5rem',
  margin: '1.5rem 0',
  padding: '1rem'
}

export default AddItem
