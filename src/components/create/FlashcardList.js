import React, { useContext } from 'react'
import FlashcardListItem from './FlashcardListItem'

import FlashcardContext from '../../context/flashcards/flashcardContext'

const FlashcardList = () => {
  const flashcardContext = useContext(FlashcardContext);
  
  const { flashcards } = flashcardContext;

  return (
    <div className="pt-2">
      {flashcards.map(flashcard => (
          <FlashcardListItem key={flashcard.id} flashcard={flashcard} />
        ))}
    </div>
  )
}

export default FlashcardList
