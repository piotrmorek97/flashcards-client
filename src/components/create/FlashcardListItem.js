import React, { useContext, useState, Fragment } from 'react'
import FlashcardContext from '../../context/flashcards/flashcardContext'

const FlashcardListItem = ({ flashcard }) => {
  const flashcardContext = useContext(FlashcardContext);
  
  const { delFlashcardItem, editFlashcardItem } = flashcardContext;

  // Edit item state
  const [edit, setEdit] = useState(false);

  // Form state
  const [state, setState] = useState({
    newTerm: '',
    newDefinition: ''
  })

  // On change form elements
  const onChange = (e) => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    })
  }

  // Edit form submission
  const editFormSubmit = (e) => {
    e.preventDefault();
    editFlashcardItem(flashcard.id, state);
    setState({
      newTerm: '',
      newDefinition: ''
    });
    // set edit -> to hide edit panel
    setEdit(false);
  }

  // Delete item
  const delItem = () => {
    // Delete item by context
    delFlashcardItem(flashcard.id);
  }

  // Edit Item
  const editItem = () => setEdit(!edit);

  const { term, definition } = flashcard;

  return (
    <Fragment>
      <div className="bg-light py-3 px-4 my-3 d-flex" style={flashcardItemStyle}>
        <div className="flashcard-text">
          <span> {term} </span>
        </div>
        <div className="flashcard-text">
          <span> {definition} </span>
        </div>
        <button style={btnStyle} className="btn btn-success mx-2 ml-auto" onClick={editItem}>
          <i className="fas fa-pen"/>
        </button>
        <button style={btnStyle} className="btn btn-danger" onClick={delItem}>
          <i className="far fa-trash-alt"/>
        </button>
      </div>
      {
        edit && 
          <form className="d-flex align-items-center" style={editForm} onSubmit={editFormSubmit}>
            <input 
              type="text" 
              name="newTerm" 
              value={state.newTerm} 
              className="form-control" 
              style={editFormInput} 
              onChange={onChange}
              placeholder="New term..."
            />
            <input 
              type="text" 
              name="newDefinition" 
              value={state.newDefinition} 
              className="form-control mx-4" 
              style={editFormInput} 
              onChange={onChange}
              placeholder="New definition..."
            />
            <input 
              type="submit" 
              value="Edit" 
              className="btn btn-light" 
              style={editFormBtn}
            />
          </form>
      }
    </Fragment>
  )
}

const flashcardItemStyle = {
  borderRadius: "0.25rem",
  boxShadow: "0.1rem 0.1rem 0.75rem 0.1rem #efefef"
}

const btnStyle = {
  height: '30px',
  width: '32px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: '#efefef'
}

const editForm = {
  width: '100%',
  border: '1px dashed #ccc',
  borderRadius: '0.5rem',
  padding: '1rem'
}

const editFormInput = {
  flex: '4'
}

const editFormBtn = {
  flex: '2',
  boxShadow: '0.1rem 0.1rem 0.75rem 0.1rem #eee'
}

export default FlashcardListItem;
