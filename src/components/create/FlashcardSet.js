import React, { useContext } from 'react'
import FlashcardList from './FlashcardList'
import AddItem from './AddItem'
import Spinner from '../layout/Spinner'

import FlashcardContext from '../../context/flashcards/flashcardContext'

const FlashcardSet = () => {
  const flashcardContext = useContext(FlashcardContext);
  
  const { loading } = flashcardContext;

  return (
    <div className="container">
      <AddItem />
      { loading && <Spinner /> }
      <FlashcardList />
    </div>
  )
}

export default FlashcardSet;
