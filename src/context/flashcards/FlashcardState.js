import React, { useReducer, useEffect } from 'react';
import axios from 'axios';
import FlashcardContext from './flashcardContext'
import FlashcardReducer from './flashcardReducer'
import {
  SET_FLASHCARDS,
  SET_LOADING,
} from '../types'

// All actions down below
const FlashcardState = (props) => {
  // Initial State
  const initialState = {
    flashcards: [],
    refresh: false,
    loading: false
  }

  const getDataFromBackend = () => {
    axios.get('https://ztiflashcardsproject.eu-gb.mybluemix.net/api/v1/flashcards')
      .then(res => {
          // dispatch to reducer
          dispatch({ type: SET_FLASHCARDS, payload: res.data });
          // set refresh to false
          console.log("Refreshed!")
        })
      .catch(e => console.log(e));
  }

  // Load data in the beginning
  useEffect(() => {
    console.log("Compunent mounted!");

    // set loading to true
    setLoading();
    
    // Loading data from backend
    getDataFromBackend();

  }, []);

  // useReducer
  const [state, dispatch] = useReducer(FlashcardReducer, initialState);

  // set loading to true
  const setLoading = () => dispatch({type: SET_LOADING});

  // Delete FlashcardItem
  const delFlashcardItem = async (id) => {
    // set loading to true
    setLoading();
    await axios.delete(`https://ztiflashcardsproject.eu-gb.mybluemix.net/api/v1/flashcards/${id}`);
    getDataFromBackend();
  }

  // Add FlashcardItem
  const addFlashcardItem = async (data) => {
    // set loading to true
    setLoading();
    await axios.post(`https://ztiflashcardsproject.eu-gb.mybluemix.net/api/v1/flashcards`, {
      term: data.term,
      definition: data.definition
    });

    getDataFromBackend();
  }

  // Edit FlashcardItem
  const editFlashcardItem = async (id, data) => {
    // set loading to true
    setLoading();
    await axios.put(`https://ztiflashcardsproject.eu-gb.mybluemix.net/api/v1/flashcards/${id}`, {
      term: data.newTerm,
      definition: data.newDefinition
    });

    getDataFromBackend();
  }

  return <FlashcardContext.Provider
    value={{
      flashcards: state.flashcards,
      loading: state.loading,
      delFlashcardItem,
      addFlashcardItem,
      editFlashcardItem
    }}
  >
    {props.children}
  </FlashcardContext.Provider>
}

export default FlashcardState;
