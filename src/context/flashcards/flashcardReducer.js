import {
  SET_LOADING,
  SET_FLASHCARDS,
} from '../types'

export default (state, action) => {
  switch(action.type) {
    case SET_FLASHCARDS:
      return {
        ...state,
        flashcards: action.payload,
        loading: false
      }
    case SET_LOADING:
      return {
        ...state,
        loading: true
      }
    default:
      return {
        ...state
      }
  }
}